#ifndef AUDIORESOURCE_H
#define AUDIORESOURCE_H

#include <QAudioOutput>
#include <QMediaPlayer>
#include <QObject>

class AudioResource : public QMediaPlayer {
    Q_OBJECT
public:
    explicit AudioResource(const QUrl &source, QObject *parent = nullptr);

    void setCustomDuration(qint64 dur);
    qint64 customDuration() const;

private:
    qint64 _customDuration = 0;
    QAudioOutput *output = new QAudioOutput;
};


#endif // AUDIORESOURCE_H
