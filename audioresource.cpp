#include "audioresource.h"

AudioResource::AudioResource(const QUrl &source, QObject *parent)
    : QMediaPlayer{parent} {
    setSource(source);
    setAudioOutput(output);

    connect(
        this, &QMediaPlayer::durationChanged, this,
        [this](qint64 dur) {
            if (!(dur > 0))
                return;
            setCustomDuration(dur);
        },
        Qt::SingleShotConnection);
}

void AudioResource::setCustomDuration(qint64 dur) { _customDuration = dur; }

qint64 AudioResource::customDuration() const { return _customDuration; }
