#include "bigletter.h"
#include <QTimer>

BigLetter::BigLetter(QWidget *parent)
    : QLabel{ parent}
{
    effect = new QGraphicsOpacityEffect(this);
    effect->setOpacity(0);
    anim = new QPropertyAnimation(effect, "opacity");
    anim->setStartValue(0);
    anim->setEndValue(1);
    anim->setDuration(0);
    setGraphicsEffect(effect);
    setAlignment(Qt::AlignCenter);
    setTextInteractionFlags(Qt::TextSelectableByMouse);
}

void BigLetter::animate(int delay)
{
    QTimer::singleShot(delay, anim, SLOT(start()));
}

void BigLetter::setFontSize(int size)
{
    QFont font = this->font();
    font.setPointSize(size);
    setFont(font);
}

void BigLetter::setFontSize(qreal size)
{
    QFont font = this->font();
    font.setPointSizeF(size);
    setFont(font);
}

qint64 BigLetter::getDesiredSize() const
{
    return desiredSize;
}

void BigLetter::setDesiredSize(qint64 size)
{
    desiredSize = size;
}

void BigLetter::reset() {
    anim->stop();
    effect->setOpacity(0);
    anim->setCurrentTime(0);
}

void BigLetter::enableTransition(bool state)
{
    anim->setDuration(state ? 1000 : 0);
}
