#include "mainwindow.h"
#include <QAudioOutput>
#include <QMediaPlayer>
#include <QTime>
#include <QVBoxLayout>
#include <QtWidgets>

int getRemainingHours() {
    QDate today = QDate::currentDate();
    return (today.daysInYear() - today.dayOfYear()) * 24;
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    qDebug("MainWindow keyPressEvent %d", event->key());
    switch (event->key()) {
    case Qt::Key_Q:
        QCoreApplication::quit();
        break;
    case Qt::Key_Return: {
        Qt::KeyboardModifiers m = event->modifiers();
        if (m & Qt::AltModifier) {
            if (this->isFullScreen()) {
                this->setWindowState(Qt::WindowMaximized);
            } else {
                this->setWindowState(Qt::WindowFullScreen);
            }
        }
    } break;
    case Qt::Key_Escape:
        if (isFullScreen())
            this->setWindowState(Qt::WindowMaximized);
        break;
    }
}

QMenu *MainWindow::createMenu() {
    auto quitAction = new QAction("&Quit", this);
    auto showMainWindowAction = new QAction("&Show", this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
    connect(showMainWindowAction, &QAction::triggered, this,
            &QMainWindow::showFullScreen);
    auto menu = new QMenu(this);
    menu->addAction(showMainWindowAction);
    menu->addAction(quitAction);

    return menu;
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason_) {
    switch (reason_) {
    case QSystemTrayIcon::DoubleClick:
    case QSystemTrayIcon::Trigger:
        // this->trayIcon->showMessage("Hello", "You clicked me!");
        setVisible(true);
        break;
    default:;
    }
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    audioDawnOfANewDay(new AudioResource(
        QUrl("qrc:/sounds/resources/sounds/Dawn-of-a-new-day.mp3"), this)),
    audioBells(new AudioResource(
        QUrl("qrc:/sounds/resources/sounds/MM_ClockTower_Bell.wav"), this)),
    audioMorning(new AudioResource(
        QUrl("qrc:/sounds/resources/sounds/OOT_Morning.wav"), this)),
    trayIcon(new QSystemTrayIcon(this)) {
    auto menu = createMenu();
    trayIcon->setContextMenu(menu);

    auto appIcon = QIcon(":/images/resources/images/icon.png");
    trayIcon->setIcon(appIcon);
    setWindowIcon(appIcon);

    trayIcon->show();

    connect(trayIcon, &QSystemTrayIcon::activated, this,
            &MainWindow::iconActivated);

    auto *centralWidget = new QWidget(this);
    auto *mainLayout = new QVBoxLayout(centralWidget);

    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);

    mainLayout->addStretch();

    setWindowTitle("Majora's Clock");

    setPalette(Qt::black);

    info->setText("Aperte O para configurações");

    topTitle->setDesiredSize(100 - 40);
    middleTitle->setDesiredSize(180 - 40);
    bottomTitle->setDesiredSize(70 - 40);

    bottomTitle->enableTransition(true);
    info->anim->setEndValue(0.25);
    info->anim->setDuration(1000);
    info->enableTransition(true);

    mainLayout->addWidget(topTitle);
    mainLayout->addWidget(middleTitle);
    mainLayout->addWidget(bottomTitle);
    mainLayout->addStretch();

    mainLayout->addWidget(info);

    setCentralWidget(centralWidget);
    setCursor(Qt::BlankCursor);

    const qint64 bellDurationMs = 17210;
    audioBells->setCustomDuration(bellDurationMs);

    const qint64 nextTime = getTimeUntilNextDay();
    if (nextTime < 0) {
        connect(audioBells,
                &QMediaPlayer::mediaStatusChanged,
                this,
                [this, nextTime](QMediaPlayer::MediaStatus e) {
                    if (e != QMediaPlayer::BufferedMedia)
                        return;
                    const qint64 position = std::ranges::min(audioBells->duration(),
                                                             std::abs(nextTime));
                    audioBells->setPosition(position);
                    audioBells->play();
                });
        audioBells->play();
        audioBells->pause();
    } else {
        setNewDayTimer(nextTime);
    }

    connect(audioBells,
            &QMediaPlayer::playbackStateChanged,
            this,
            [=, this](QMediaPlayer::PlaybackState e) {
                if (!(e == QMediaPlayer::StoppedState))
                    return;
                show();
                setNewDayTimer(getTimeUntilNextDay());
                audioBells->setPosition(0);
            });
}

void MainWindow::showEvent(QShowEvent *) {
    // setTimeout to delay audio start, to make it more like the original
    // because there's a slight delay between the titles showing
    // and the bang sound
    static const int magicNumber = 800;
    QTimer::singleShot(magicNumber, audioDawnOfANewDay, [=, this] {
        audioDawnOfANewDay->play();
        if (QTime::currentTime().hour() < 12)
            audioMorning->play();
    });
    resizeFonts();
    showTitles();
    QTimer::singleShot(10000, this, [=, this] {
        hide();
        topTitle->reset();
        middleTitle->reset();
        bottomTitle->reset();
        info->reset();
    });
}

qint64 MainWindow::getTimeUntilNextDay()
{
    static const qint64 bellDurationMs = audioBells->customDuration();
    const QDateTime now = QDateTime::currentDateTime();
    auto nextTime = QDateTime(now);
#ifdef QT_DEBUG
    nextTime.setTime(QTime(now.time().hour(), now.time().minute() + 1, 0, 0));
#else
    if (now.time().hour() < 6) {
        nextTime.setTime(QTime(6, 0, 0, 0));
    } else if (now.time().hour() < 18) {
        nextTime.setTime(QTime(18, 0, 0, 0));
    } else {
        nextTime = nextTime.addDays(1);
        nextTime.setTime(QTime(6, 0, 0, 0));
    }
#endif
    const qint64 nextBell = now.msecsTo(nextTime);
    if (nextBell < 0)
        return nextBell;
    const qint64 timeout = nextBell - bellDurationMs;

    return timeout;
}

void MainWindow::setNewDayTimer(qint64 timeout) {
    static const qint64 bellDurationMs = audioBells->customDuration();
    static const int smallDelayMs = 1000;
    QTimer::singleShot(timeout + smallDelayMs, audioBells, SLOT(play()));
}

void MainWindow::showTitles() {
    const int dayOfYear = QDate::currentDate().dayOfYear();

    topTitle->setText(
        QString("%1 of").arg(QTime::currentTime().hour() < 12 ? "Dawn" : "Fall"));

    QString middleTitleText = QString("The %1 Day");

    switch (dayOfYear) {
    case 1:
        middleTitleText = middleTitleText.arg("First");
        break;
    case 2:
        middleTitleText = middleTitleText.arg("Second");
        break;
    case 3:
        middleTitleText = middleTitleText.arg("Third");
        break;
    default:
        const QDate today = QDate::currentDate();
        const bool isLastDay = (today.daysInYear() - today.dayOfYear()) == 0;
        if (isLastDay)
            middleTitleText = middleTitleText.arg("Final");
        else
            middleTitleText = middleTitleText.arg(QString("%1th").arg(dayOfYear));
        break;
    }

    middleTitle->setText(middleTitleText);

    bottomTitle->setText(QString("- %1 Hours Remain -").arg(getRemainingHours()));

    topTitle->animate(2000);
    middleTitle->animate(2000);
    bottomTitle->animate(3000);
    info->animate(0);
}

void MainWindow::resizeFonts()
{
    const std::array<BigLetter *, 3> bigLetters = {
        topTitle,
        middleTitle,
        bottomTitle,
    };

    for (auto bigLetter : bigLetters) {
        qreal newFontSize = (qreal(bigLetter->getDesiredSize()) / qreal(1536)) * width();

        bigLetter->setFontSize(newFontSize);
    }
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    resizeFonts();
}

MainWindow::~MainWindow() {}
