#ifndef BIGLETTER_H
#define BIGLETTER_H

#include <QWidget>
#include <QPropertyAnimation>
#include <QLabel>
#include <QGraphicsOpacityEffect>

class BigLetter : public QLabel
{
    Q_OBJECT
public:
    explicit BigLetter(QWidget *parent = nullptr);

    QPropertyAnimation *anim;
    QGraphicsOpacityEffect *effect;

    void animate(int delay = 1000);

    void setFontSize(int size);
    void setFontSize(qreal size);
    void enableTransition(bool);
    void reset();
    qint64 getDesiredSize() const;
    void setDesiredSize(qint64);

private:
    qint64 desiredSize;
};

#endif // BIGLETTER_H
