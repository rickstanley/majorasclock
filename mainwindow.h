#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "audioresource.h"
#include "bigletter.h"
#include <QMainWindow>
#include <QMediaPlayer>
#include <QMenu>
#include <QSystemTrayIcon>

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void keyPressEvent(QKeyEvent *event) override;
    void showWindow();

protected:
    // void closeEvent(QCloseEvent *event) override;
    void showEvent(QShowEvent *) override;
    void resizeEvent(QResizeEvent *) override;

public slots:
    void iconActivated(QSystemTrayIcon::ActivationReason);
    void setNewDayTimer(qint64 timeout);

private:
    BigLetter *topTitle = new BigLetter;
    BigLetter *middleTitle = new BigLetter;
    BigLetter *bottomTitle = new BigLetter;
    BigLetter *info = new BigLetter;

    AudioResource *audioDawnOfANewDay;
    AudioResource *audioBells;
    AudioResource *audioMorning;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    QMenu *createMenu();

    qint64 getTimeUntilNextDay();

    void resizeFonts();

    void showTitles();
};
#endif // MAINWINDOW_H
